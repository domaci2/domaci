CREATE DATABASE  IF NOT EXISTS `domaci_ADAMVASIC` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `domaci_ADAMVASIC`;
-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: domaci_ADAMVASIC
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `drzava`
--

DROP TABLE IF EXISTS `drzava`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drzava` (
  `kod` varchar(3) NOT NULL,
  `naziv` varchar(45) NOT NULL,
  PRIMARY KEY (`kod`),
  UNIQUE KEY `kod_UNIQUE` (`kod`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drzava`
--

LOCK TABLES `drzava` WRITE;
/*!40000 ALTER TABLE `drzava` DISABLE KEYS */;
INSERT INTO `drzava` VALUES ('101','Srbija'),('102','Slovenija'),('103','Hrvatska');
/*!40000 ALTER TABLE `drzava` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grad`
--

DROP TABLE IF EXISTS `grad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grad` (
  `pttBroj` varchar(10) NOT NULL,
  `naziv` varchar(45) NOT NULL,
  `drzavaID` varchar(3) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_grad_1_idx` (`drzavaID`),
  CONSTRAINT `fk_grad_1` FOREIGN KEY (`drzavaID`) REFERENCES `drzava` (`kod`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grad`
--

LOCK TABLES `grad` WRITE;
/*!40000 ALTER TABLE `grad` DISABLE KEYS */;
INSERT INTO `grad` VALUES ('11000','Beograd','101',1),('21000','Novi Sad','101',2),('18000','Nis','101',3),('34000','Kragujevac','101',4),('1000','Ljubljana','102',5),('2000','Maribor','102',6),('2250','Ptuj','102',7),('3000','Celje','102',8),('10000','Zagreb','103',9),('21000','Split','103',10),('20000','Dubrovnik','103',11),('51000','Rijeka','103',12);
/*!40000 ALTER TABLE `grad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategorija`
--

DROP TABLE IF EXISTS `kategorija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategorija` (
  `kategorijaID` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) NOT NULL,
  PRIMARY KEY (`kategorijaID`),
  UNIQUE KEY `kategorijaID_UNIQUE` (`kategorijaID`)
) ENGINE=InnoDB AUTO_INCREMENT=1012 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategorija`
--

LOCK TABLES `kategorija` WRITE;
/*!40000 ALTER TABLE `kategorija` DISABLE KEYS */;
INSERT INTO `kategorija` VALUES (1000,'Namirnice'),(1001,'Mlecni Proizvodi'),(1002,'Voce i Povrce'),(1003,'Meso i Riba'),(1004,'Smrznuto'),(1005,'Pice'),(1006,'Slatkisi'),(1007,'Grickalice'),(1008,'Licna Higijena'),(1009,'Kucna Hemija'),(1010,'Kutak za Bebe'),(1011,'Kucni Ljubimci');
/*!40000 ALTER TABLE `kategorija` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodavnica`
--

DROP TABLE IF EXISTS `prodavnica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodavnica` (
  `prodavnicaID` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) NOT NULL,
  `gradID` int(11) NOT NULL,
  PRIMARY KEY (`prodavnicaID`),
  UNIQUE KEY `prodavnicaID_UNIQUE` (`prodavnicaID`),
  KEY `fk_prodavnica_1_idx` (`gradID`),
  CONSTRAINT `fk_prodavnica_1` FOREIGN KEY (`gradID`) REFERENCES `grad` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodavnica`
--

LOCK TABLES `prodavnica` WRITE;
/*!40000 ALTER TABLE `prodavnica` DISABLE KEYS */;
INSERT INTO `prodavnica` VALUES (120,'Maxi',1),(121,'Maxi',2),(122,'Maxi',3),(123,'Maxi',4),(124,'Idea',1),(125,'Idea',2),(126,'Idea',3),(127,'Idea',4),(128,'Mercator',5),(129,'Mercator',6),(130,'Mercator',7),(131,'Mercator',8),(132,'Spar',5),(133,'Spar',6),(134,'Spar',7),(135,'Spar',8),(136,'Idea',9),(137,'Idea',10),(138,'Idea',11),(139,'Idea',12);
/*!40000 ALTER TABLE `prodavnica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodavnica_proizvod`
--

DROP TABLE IF EXISTS `prodavnica_proizvod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodavnica_proizvod` (
  `proizvodID` int(11) NOT NULL,
  `prodavnicaID` int(11) NOT NULL,
  `cena` decimal(12,2) NOT NULL,
  KEY `fk_prodavnica_proizvod_1_idx` (`proizvodID`),
  KEY `fk_prodavnica_proizvod_2_idx` (`prodavnicaID`),
  CONSTRAINT `fk_prodavnica_proizvod_1` FOREIGN KEY (`proizvodID`) REFERENCES `proizvod` (`proizvodID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_prodavnica_proizvod_2` FOREIGN KEY (`prodavnicaID`) REFERENCES `prodavnica` (`prodavnicaID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodavnica_proizvod`
--

LOCK TABLES `prodavnica_proizvod` WRITE;
/*!40000 ALTER TABLE `prodavnica_proizvod` DISABLE KEYS */;
INSERT INTO `prodavnica_proizvod` VALUES (1,121,154.99),(3,121,47.99),(5,121,119.99),(8,121,65.99),(12,121,89.99),(15,121,159.99),(18,121,45.99),(19,121,78.99),(21,121,219.99),(23,121,54.99),(26,121,78.99);
/*!40000 ALTER TABLE `prodavnica_proizvod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proizvod`
--

DROP TABLE IF EXISTS `proizvod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proizvod` (
  `proizvodID` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) NOT NULL,
  `barkod` varchar(13) NOT NULL,
  `proizvodjacID` int(11) NOT NULL,
  `kategorijaID` int(11) NOT NULL,
  PRIMARY KEY (`proizvodID`),
  UNIQUE KEY `proizvodID_UNIQUE` (`proizvodID`),
  KEY `fk_proizvod_1_idx` (`proizvodjacID`),
  KEY `fk_proizvod_2_idx` (`kategorijaID`),
  CONSTRAINT `fk_proizvod_1` FOREIGN KEY (`proizvodjacID`) REFERENCES `proizvodjac` (`proizvodjacID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_proizvod_2` FOREIGN KEY (`kategorijaID`) REFERENCES `kategorija` (`kategorijaID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proizvod`
--

LOCK TABLES `proizvod` WRITE;
/*!40000 ALTER TABLE `proizvod` DISABLE KEYS */;
INSERT INTO `proizvod` VALUES (1,'Brasno T-400 1kg','8601300046013',1,1000),(2,'Brasno T-500 1kg','8601300046006',1,1000),(3,'Brasno T-400 1kg','8600949000158',2,1000),(4,'Brasno T-500 1kg','8600949000165',2,1000),(5,'Jaja klasa L 10kom','8600197405071',3,1000),(6,'Jaja klasa L 30kom','8600197405061',3,1000),(7,'Kecap Blagi pvc 1kg','8600498150090',7,1000),(8,'Kecap Ljuti pvc 500g','8600498150106',7,1000),(9,'Kecap 1kg','8715700421520',8,1000),(10,'Kecap 570g','8715700421575',8,1000),(11,'Margarin Classic 250g','8600498172115',7,1000),(12,'Margarin Dobro Jutro 250g','8600498170418',7,1000),(13,'Pirinac Dugo Zrno 1kg','8607100137456',9,1000),(14,'Pirinac Basmati 4x125g','5941832000859',10,1000),(15,'Secer 1kg','8600101827043',11,1000),(16,'Secer 1kg','8606103146014',12,1000),(17,'Alkoholno Sirce 1L','8600030004706',13,1000),(18,'Alkoholno Sirce 1L','8600498150274',7,1000),(19,'Fusilli n.98 500g','8076802085981',14,1000),(20,'Pene Rigate n.73 500g','8076802085738',14,1000),(21,'Farfalle n.65 500g','8076808060654',14,1000),(22,'Makaroni Fusilli Integrale 400g','8601300044286',1,1000),(23,'Ulje 1L','8600498170029',7,1000),(24,'Ulje 5L','8600498170067',7,1000),(25,'Ulje Omegol 1L','8600498170081',7,1000),(26,'Maslinovo Ulje extra 500ml','8005510161106',15,1000);
/*!40000 ALTER TABLE `proizvod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proizvodjac`
--

DROP TABLE IF EXISTS `proizvodjac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proizvodjac` (
  `proizvodjacID` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(100) NOT NULL,
  `gradID` int(11) NOT NULL,
  PRIMARY KEY (`proizvodjacID`),
  UNIQUE KEY `proizvodjacID_UNIQUE` (`proizvodjacID`),
  KEY `fk_proizvodjac_1_idx` (`gradID`),
  CONSTRAINT `fk_proizvodjac_1` FOREIGN KEY (`gradID`) REFERENCES `grad` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proizvodjac`
--

LOCK TABLES `proizvodjac` WRITE;
/*!40000 ALTER TABLE `proizvodjac` DISABLE KEYS */;
INSERT INTO `proizvodjac` VALUES (1,'Danubius',2),(2,'Stari Mlinar',1),(3,'Vin Farm',1),(7,'Dijamant',3),(8,'Heinz',4),(9,'C',4),(10,'Scotti',1),(11,'Crvenka',2),(12,'Sunoko',2),(13,'BIP',4),(14,'Barilla',1),(15,'Monini',1);
/*!40000 ALTER TABLE `proizvodjac` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-19 11:38:18
